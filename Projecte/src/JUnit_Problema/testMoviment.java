package JUnit_Problema;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import Problema.Problema;;

@RunWith(Parameterized.class)

public class testMoviment {
	private char[][] secret;
	private int fila;
	private int columna;
	private int[] stats;

	public testMoviment(char[][] secreto, int fila, int columna, int[] stats) {
		this.secret = secreto;
		this.fila = fila;
		this.columna = columna;
		this.stats = stats;
	}

	@Parameters
	public static Collection<Object[]> numeros() {
		return Arrays.asList(new Object[][] {
				
				 {new char[][]{{'T','P','T'},{'P','T','T'}},1, 1,new int[]{5,5}},
				 {new char[][]{{'T','T','T'},{'T','T','T'}},0, 2,new int[]{-5,0}},
				 {new char[][]{{'P','P','P'},{'P','P','P'}},1, 2,new int[]{5,5}},
				 {new char[][]{{'P','P','T'},{'T','T','P'}},0, 2,new int[]{5,5}},
				 {new char[][]{{'T','T','T'},{'P','P','T'}},0, 0,new int[]{-5,0}}
		});
	}

	@Test
	public void testStat() {
		int[] statstest = Problema.comprovarcasilla(secret, fila, columna, stats);
		assertEquals(statstest, stats);
		// fail("Not yet implemented");
	}

}