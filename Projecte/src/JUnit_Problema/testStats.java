package JUnit_Problema;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import Problema.Problema;

@RunWith(Parameterized.class)

public class testStats {
	int[]stats;
	int[]jugador;

	public testStats(int[]stats,int[]jugador) {
		this.stats=stats;
		this.jugador=jugador;
	}

	@Parameters
	public static Collection<Object[]> numeros() {
		return Arrays.asList(new Object[][] {
			{new int[]{40,60}, new int[] {140,70}},
			{new int[]{25,-20}, new int[] {125,-10}},
			{new int[]{-10,-5}, new int[] {90,5}},
			{new int[]{20,2}, new int[] {120,12}},
			{new int[]{-50,100}, new int[] {50,110}}		
		});
	}

	@Test
	public void testStat() {
		int[] statstest = Problema.estadisticas(stats, jugador);
		assertEquals(statstest, jugador);
		// fail("Not yet implemented");
	}

}
