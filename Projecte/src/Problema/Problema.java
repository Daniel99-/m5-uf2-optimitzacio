package Problema;

import java.util.Scanner;

public class Problema {

	static Scanner reader = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		char[][] tauler;
		char[][] secret;
		int casos = 0;

		int[] stats = new int[2];
		int[] jugador = new int[2];

		casos = reader.nextInt();
		reader.nextLine();
		for (int i = 0; i < casos; i++) {
			tauler = inicialitzarTauler();
			// visualitzarTauler(tauler);
			secret = inicialitzarSecret();
			moviments(tauler, secret, stats);
			torn(stats, jugador);
			stats[0] = 0;
			stats[1] = 0;
		}
	}

	/**
	 * En aquest metode estem creant el tauler amb una medida de 6x6
	 * 
	 * @return retorna el tauler
	 */
	public static char[][] inicialitzarTauler() {

		char[][] tauler = new char[6][6];

		for (int i = 0; i < tauler.length; i++) {
			for (int j = 0; j < tauler.length; j++) {
				if (i == 5 && j == 3) {
					tauler[i][j] = 'J';
				} else if (i == 0 && j == 3) {
					tauler[i][j] = 'E';
				} else {
					tauler[i][j] = '#';
				}
			}
		}

		return tauler;
	}

	/**
	 * En aquest metode estic amagant els potencials i les trampes que hi ha al
	 * voltant pel cami
	 * 
	 * @return retorna el secret
	 */
	public static char[][] inicialitzarSecret() {
		char[][] secret = { { 'T', 'P', 'T', 'E', 'T', 'T' }, { 'P', 'T', 'T', 'P', 'T', 'P' },
				{ 'T', 'P', 'T', 'T', 'P', 'T' }, { 'P', 'T', 'P', 'T', 'P', 'T' }, { 'P', 'T', 'T', 'P', 'T', 'P' },
				{ 'T', 'P', 'T', 'J', 'P', 'T' } };

		return secret;
	}


	/**
	 * 
	 * @param stats mostra el parametre dels stats
	 * @param jugador mostra el parametre del jugador
	 * @return retorna les estadistiques del jugador
	 */
	public static int[] estadisticas(int[] stats, int[] jugador) {
		jugador[0] = 100;
		jugador[1] = 10;

		jugador[0] += stats[0];
		jugador[1] += stats[1];

		return jugador;
	}

	/**
	 * En aquest metode mostra les estadistiques del enemig
	 * 
	 * @return retorna les estadistiques del enemig
	 */
	public static int[] estenemig() {
		int[] enemig = new int[2];
		enemig[0] = 125;
		enemig[1] = 20;

		return enemig;
	}

	/**
	 * En aquest metode es on fa els moviments el jugador i acaba de moure quant
	 * arribi al enemig
	 * 
	 * @param tauler mostra el parametre del tauler
	 * @param secret mostra el parametre del secret
	 * @param stats  mostra el parametre de les estadistiques del jugador
	 */
	public static void moviments(char[][] tauler, char[][] secret, int[] stats) {

		char direccio;
		int fila = 5;
		int columna = 3;

		while (secret[fila][columna] != 'E') {
			direccio = reader.next().charAt(0);
			direccio = Character.toLowerCase(direccio);
			if (direccio == 'd') {
				tauler[fila][columna + 1] = tauler[fila][columna];
				tauler[fila][columna] = '#';
				columna = columna + 1;

			} else if (direccio == 'e') {
				tauler[fila][columna - 1] = tauler[fila][columna];
				tauler[fila][columna] = '#';
				columna = columna - 1;

			} else if (direccio == 'a') {
				tauler[fila - 1][columna] = tauler[fila][columna];
				tauler[fila][columna] = '#';
				fila = fila - 1;

			} else if (direccio == 'b') {
				tauler[fila + 1][columna] = tauler[fila][columna];
				tauler[fila][columna] = '#';
				fila = fila + 1;

			}
			comprovarcasilla(secret, fila, columna, stats);
		}

	}

	/**
	 * @param secret  mostra el parametre del secret
	 * @param fila    mostra el parametre de la fila
	 * @param columna mostra el parametre de la columna
	 * @param stats   mostra el parametre de les estadistiques del jugador
	 * @return retorna les estadistiques del jugador
	 */
	public static int[] comprovarcasilla(char[][] secret, int fila, int columna, int[] stats) {
		if (secret[fila][columna] == 'T') {
			stats[0] = stats[0] - 5;
		} else if (secret[fila][columna] == 'P') {
			stats[0] = stats[0] + 5;
			stats[1] = stats[1] + 5;
		}
		return stats;
	}

	/**
	 * En aquest metode es el torn de la lluita entre el jugador i el enemig on el
	 * primer en atacar es el jugador i la lluita acaba quant el jugador o el drac
	 * tinguin la vida a 0 i mostra si el jugador a matat al enemig o el enemig a
	 * matat al enemig
	 * 
	 * @param statsjugador mostra el parametre dels stats del jugador
	 * @param jugador mostra el parametre del jugador
	 */
	public static void torn(int[] statsjugador, int[] jugador) {
		int[] total = new int[2];
		int[] totalenemig = new int[2];
		total = estadisticas(statsjugador, jugador);
		totalenemig = estenemig();
		while (total[0] > 0 && totalenemig[0] > 0) {
			totalenemig[0] = totalenemig[0] - total[1];
			if (totalenemig[0] > 0) {
				total[0] = total[0] - totalenemig[1];
			}

		}
		if (totalenemig[0] <= 0) {
			System.out.println("EL LOTHRIC HA MATAT AL DRAC");
		} else {
			System.out.println("EL DRAC HA MATAT AL LOTHRIC");
		}
	}

}